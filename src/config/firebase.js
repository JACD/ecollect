// import firebase from "firebase/app";
// import "firebase/auth";
// import "firebase/firestore";

// const firebaseConfig = {
// 	apiKey: "AIzaSyD2V7IyIhyoFKzo_VhTk-OjymPuFsRXVfc",
// 	authDomain: "baliok-9510a.firebaseapp.com",
// 	databaseURL: "https://baliok-9510a.firebaseio.com",
// 	projectId: "baliok-9510a",
// 	storageBucket: "baliok-9510a.appspot.com",
// 	messagingSenderId: "434860840988",
// 	appId: "1:434860840988:web:2ed318fa70942ee4c767d9",
// 	measurementId: "G-XX1TV879Y6",
// };

// firebase.initializeApp(firebaseConfig);
// export const auth = firebase.auth();
// export const firestore = firebase.firestore();

import firebase from "firebase/app";
import "firebase/auth";

const app = firebase.initializeApp({
	apiKey: "AIzaSyD2V7IyIhyoFKzo_VhTk-OjymPuFsRXVfc",
	authDomain: "baliok-9510a.firebaseapp.com",
	databaseURL: "https://baliok-9510a.firebaseio.com",
	projectId: "baliok-9510a",
	storageBucket: "baliok-9510a.appspot.com",
	messagingSenderId: "434860840988",
});

export default app;
