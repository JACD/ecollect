import React, { useEffect, useState } from "react";
import app from "./firebase.js";

export const AuthContext = React.createContext();

export const AuthProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null);
  const [pending, setPending] = useState(true);

  // useEffect(() => {
  //   app.auth().onAuthStateChanged(setCurrentUser);
  // }, []);
  useEffect(() => {
    app.auth().onAuthStateChanged((user) => {
      setTimeout(() => {
        setCurrentUser(user);
      }, 2);
      //setCurrentUser(user);
      setPending(false);
    });
  }, []);
  // app.auth().onAuthStateChanged(function (user) {
  //   if (user) {
  //     setCurrentUser(user);
  //   } else {
  //     setPending(false);
  //   }
  // });

  if (pending) {
    return <div data-uk-spinner className="uk-position-center"></div>;
  }

  return (
    <AuthContext.Provider
      value={{
        currentUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
