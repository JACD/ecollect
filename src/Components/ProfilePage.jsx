import React from "react";
import app from "../config/firebase.js";
//import { AuthContext } from "../config/Auth";
const ProfilePage = () => {
	const inputRef = React.useRef(null);

	function handleClick(e) {
		e.preventDefault();
		inputRef.current.click();
	}

	function UserGreeting(props) {
		return <h1>Welcome back!</h1>;
	}

	function GuestGreeting(props) {
		return <h1>Please sign up.</h1>;
	}
	function Greeting(props) {
		const currentUser = props.currentUser;
		if (currentUser) {
			return <UserGreeting />;
		}
		return <GuestGreeting />;
	}

	// function asdfasdfasdf(props) {
	// 	const currentUser = props.currentUser;
	// 	if (currentUser) {
	// 		return <handleClick />;
	// 	}
	// }

	// if (currentUser) {
	// 	return <UserGreetings />;
	// }

	return (
		<div
			className="mx-auto w-11/12 md:w-2/4 py-8 px-4 md:px-8"
			ref={inputRef}
			onClick={() => console.log("clicked")}
		>
			<div className="flex border flex-col items-center md:flex-row md:items-start border-blue-400 px-3 py-4">
				<div className="border border-blue-300"></div>
				<div className="md:pl-4">
					<Greeting currentUser={true} />
					<h2 className="text-2xl font-semibold">Home </h2>
				</div>
			</div>
			<button
				className="w-full py-3 bg-red-600 mt-4 text-white"
				onClick={() => app.auth().signOut()}
			>
				Sign out
			</button>
		</div>
	);
};
export default ProfilePage;
