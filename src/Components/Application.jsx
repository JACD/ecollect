import React from "react";
import Header from "../landingpage/Header";
import Nav from "../landingpage/Navigation";
import Footer from "../landingpage/Footer";

function Application() {
	return (
		<React.Fragment>
			<Nav />
			<Header />
			<Footer />
		</React.Fragment>
	);
}
export default Application;
