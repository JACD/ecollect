import React, { Component } from "react";
class History extends Component {
	render() {
		return (
			<div className="uk-section uk-section-muted" id="hist">
				<div className="uk-container">
					<article className="uk-article">
						<span
							data-uk-icon="icon: history; ratio: 2"
							className="heartclr"
						></span>
						<h1 className="uk-article-title">
							<a className="uk-link-reset" href="#hist">
								History/Background
							</a>
						</h1>

						<p>
							Barangay Baliok was created under a Commonwealth Act
							of 1937 and was inaugurated, March 1, 1951. They
							elected their first Tenyente Del Barrio, the late
							Eugenio Naraval, and the Vice Tenyente del Barrio,
							Mr. Floro C. Parba. By June 19, 1965, by virtue of
							RA No. 4354, otherwise known as the Revised Charter
							of Davao City, Barangay Baliok was again recreated.
							It was named after a big tree that grows within the
							vicinity having a common name – Bayok, with a
							specific name “Pterospermum Diversifolium Blume.”
							The Bagobos, the dominant tribe during that time,
							uses the bark of this tree for coloring. Later on,
							from the word Bayok, the Bagobos called it Baliok.
						</p>

						<p>
							On March 19, 2003, the Barangay Council of Baliok,
							under the leadership of our present Barangay Rudolfo
							M. Bagajo. Sr., passed a Resolution No. 31 series of
							2003, requesting the Honorable Mayor Rodrigo Roa
							Duterte to issue a Proclamation commemorating June
							19 as Araw ng Barangay Baliok.
						</p>

						<p>
							On April 14, 2003, the Hon. City Mayor Rodrigo Roa
							Duterte issued a Proclamation No. 6 Declaring June
							19 and every year after that as Araw ng Barangay
							Baliok. We celebrated our First Araw last June 19,
							2003, commemorating the 38th years of existence of
							Barangay Baliok. On October 29, 2007, Barangay
							Election, Antonio A. Tinasas won as a Barangay
							Captain, which he was a Barangay Kagawad of Baliok
							for Twenty Five (25) Years.
						</p>

						<p>
							Barangay Baliok has a total land area of 306
							hectares. Bounded on the North by Barangay Bago
							Gallera: on the South by Barangay Lubogan: On the
							West by Barangay Bangkas Heights and on the East by
							Barangay Dumoy.
						</p>

						<p>
							Barangay Baliok, celebrated the annual Fiesta, every
							29th day of August with a Patron Saint, St.
							Agustine.
						</p>
					</article>
				</div>
			</div>
		);
	}
}

export default History;
