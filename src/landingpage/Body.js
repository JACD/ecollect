import React, { Component } from "react";
class vm extends Component {
    render() {
        return (
            <div
                className="uk-grid-divider uk-child-width-expand@s uk-section-muted uk-padding-large"
                id="about-us"
                data-uk-grid
            >
                <div className="uk-text-center">
                    <span
                        data-uk-icon="icon: heart; ratio: 2"
                        className="heartclr"
                    ></span>
                    <h3 className="uk-text-bolder">VISION</h3>
                    <p>
                        The Barangay stands as a beacon to lift its constituency
                        in an improved and dignified quality of life.
                    </p>
                </div>
                <div className="uk-text-center">
                    <span
                        data-uk-icon="icon: heart; ratio: 3"
                        className="heartclr"
                    ></span>
                    <h3 className="uk-text-bolder">MISSION</h3>
                    <p>
                        The Barangay, as a conduit of the government in nation
                        building, inspired by Filipino patriotism, is committed
                        to fully develop the human resource of its well-balanced
                        ecology and appropriate technological complementing
                        facilities. In attaining such endeavor, its network of
                        livelihood and revitalized economic enterprise, through
                        free access to faith maintenance of enhanced vital
                        social services. In this light, it promotes an empirical
                        linkage of a partnership between the barangay and the
                        international communities towards the propagation of
                        ideal and progressive habitat.
                    </p>
                </div>
            </div>
        );
    }
}

export default vm;
