import React, { Component } from "react";
import { Imgcaptain } from "./Images";
class Captain extends Component {
	render() {
		return (
			<div className="uk-container uk-padding" id="bc">
				<div className="uk-text-center">
					<h2 className="uk-text-bolder">BARANGAY CAPTAIN</h2>
					<span
						data-uk-icon="icon: chevron-down; ratio: 2"
						className="heartclr uk-padding uk-padding-remove-top"
					></span>
				</div>
				<div
					className="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s"
					data-uk-grid
				>
					<div className="uk-card-media-left uk-cover-container">
						<Imgcaptain />
						<canvas width="600" height="400"></canvas>
					</div>
					<div>
						<div className="uk-card-body">
							<h3 className="uk-card-title uk-text-bolder">
								<span
									data-uk-icon="icon: user; ratio: 1.5"
									className="heartclr"
								>
									Hon. lorem Ipsum
								</span>{" "}
							</h3>
							<h5>Year Declared: July 9, 1990</h5>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Captain;
