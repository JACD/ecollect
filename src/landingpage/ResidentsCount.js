import React, { Component } from "react";
class Count extends Component {
	render() {
		return (
			<div className="uk-bg-green uk-text-center" id="popu">
				<div
					className="uk-child-width-1-3@m uk-grid-small uk-grid-match uk-padding-large"
					data-uk-grid
				>
					<div>
						<div
							className="uk-card uk-card-body"
							width="200"
							height="200"
						>
							<h2 className="clr">All residents</h2>
							<hr className="uk-divider-small" />
							<h3 className="clr"> 700 </h3>
						</div>
					</div>
					<div>
						<div
							className="uk-card uk-card-body clr"
							width="200"
							height="200"
						>
							<h2 className="clr">Male</h2>
							<hr className="uk-divider-small" />
							<h3 className="clr"> 500</h3>
						</div>
					</div>
					<div>
						<div
							className="uk-card uk-card-body clr"
							width="200"
							height="200"
						>
							<h2 className="clr">Female</h2>
							<hr className="uk-divider-small" />
							<h3 className="clr"> 200</h3>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Count;
