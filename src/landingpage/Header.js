import React, { Component } from "react";
import Imgheader from "./Images";
import VisiionMission from "./Body";
import Barangayofficial from "./Official";
import History from "./History";
import Captain from "./Captain";
import Count from "./ResidentsCount";

class App extends Component {
    render() {
        return (
            <div>
                <div className="uk-padding-left uk-padding-right">
                    <div className="uk-cover-container uk-height-large">
                        <Imgheader />
                        <div className="uk-position-center uk-text-center">
                            <article
                                className="uk-article uk-padding"
                                id="article-style"
                            >
                                <h1 className="uk-article-title uk-text-bolder">
                                    WELCOME BARANGAY BALIOK
                                </h1>
                                <p className="uk-article-meta uk-text-small">
                                    Visit the Barangay Baliok for more
                                    information just contact Mrs. Lolita L.
                                    Marin.
                                </p>
                            </article>
                            <a href="#about-us" data-uk-scroll>
                                {" "}
                                <button
                                    className="uk-button uk-button-primary uk-button-large uk-margin uk-visible@s uk-animation-scale-up uk-transform-origin-bottom-right"
                                    id="btn-more-l"
                                >
                                    <span>MORE INFO</span>
                                </button>
                            </a>
                            <a href="#about-us" data-uk-scroll>
                                {" "}
                                <button
                                    className="uk-button uk-button-primary uk-button-meduim uk-margin uk-hidden@s uk-animation-scale-up uk-transform-origin-bottom-right"
                                    id="btn-more-lg"
                                >
                                    <span>MORE INFO</span>
                                </button>
                            </a>
                        </div>
                    </div>
                    <VisiionMission />
                    <Barangayofficial />
                    <History />
                    <Captain />
                    <Count />
                </div>
            </div>
        );
    }
}

export default App;
