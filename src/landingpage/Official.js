import React, { Component } from "react";
import { Imguser } from "./Images";
class brgyofficial extends Component {
    render() {
        return (
            <div className="uk-container uk-padding" id="brgyoff">
                <div className="uk-text-center">
                    <h2 className="uk-text-bolder">BARANGAY OFFICIALS</h2>
                    <span
                        uk-icon="icon: chevron-down; ratio: 2"
                        className="heartclr uk-padding uk-padding-remove-top"
                    ></span>
                </div>
                <div
                    className="uk-grid-column-small uk-grid-row-large uk-child-width-1-3@s uk-text-center uk-margin-left uk-margin-right"
                    data-uk-grid
                >
                    <div>
                        <div className="uk-card uk-card-default">
                            <div className="uk-card-media-top">
                                <Imguser />
                            </div>
                            <div className="uk-card-body">
                                <span
                                    uk-icon="icon: user; ratio: 1.5"
                                    className="heartclr"
                                ></span>
                                <h3 className="uk-card-title uk-text-bolder">
                                    Lorem Ipsum
                                </h3>
                                <h4>Rank: Rank 1</h4>
                                <h5>Year Declared:</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default brgyofficial;
