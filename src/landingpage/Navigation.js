import React, { Component } from "react";
import { ImgLogo1, ImgLogo2 } from "./Images";
import LoginModal from "../modal/Modal";
import SignupModal from "../modal/signup";

class Nav extends Component {
    render() {
        return (
            <div className="site-branding">
                <div
                    className="uk-visible@m"
                    uk-sticky="animation: uk-animation-slide-top sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar"
                >
                    <nav
                        className="uk-navbar-container navbar-style"
                        data-uk-navbar
                    >
                        <div className="uk-navbar-left uk-animation-toggle">
                            <a
                                className="uk-navbar-item uk-logo uk-animation-scale-up"
                                href="/index.html"
                            >
                                <ImgLogo1 />
                            </a>
                        </div>
                        <div className="uk-navbar-right">
                            <ul className="uk-navbar-nav">
                                <li>
                                    <a
                                        className="nav-link"
                                        href="#about-us"
                                        data-uk-scroll
                                    >
                                        ABOUT US
                                    </a>
                                </li>
                                <li>
                                    <a
                                        className="nav-link"
                                        href="#brgyoff"
                                        data-uk-scroll
                                    >
                                        BARANGAY OFFICIALS{" "}
                                    </a>
                                </li>
                                <li>
                                    <a
                                        className="nav-link"
                                        href="#hist"
                                        data-uk-scroll
                                    >
                                        HISTORY{" "}
                                    </a>
                                </li>
                                <li>
                                    <a
                                        className="nav-link"
                                        href="#bc"
                                        data-uk-scroll
                                    >
                                        BARANGAY CAPTAIN
                                    </a>
                                </li>
                                <li>
                                    <a
                                        className="nav-link"
                                        href="#popu"
                                        data-uk-scroll
                                    >
                                        POPULATION
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="uk-navbar-item">
                            <button
                                className="uk-button uk-button-primary btn-login"
                                data-uk-toggle="target: #modal-group-1"
                            >
                                Login
                            </button>
                        </div>
                    </nav>
                </div>
                {/*For mobile*/}
                <div className="uk-hidden@m">
                    <div data-uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky">
                        <nav className="uk-navbar uk-navbar-container navbar-style">
                            <div className="uk-navbar-left">
                                <a
                                    className="uk-navbar-toggle"
                                    data-uk-toggle="target: #offcanvas-nav-primary"
                                    href="#toggle"
                                >
                                    <span data-uk-navbar-toggle-icon></span>{" "}
                                    <span className="uk-margin-small-left"></span>
                                </a>
                            </div>
                        </nav>
                    </div>
                </div>

                <div
                    id="offcanvas-nav-primary"
                    data-uk-offcanvas="overlay: true"
                >
                    <div
                        className="uk-offcanvas-bar uk-flex uk-flex-column uk-background-default"
                        id="mob-nav-style"
                    >
                        <ul className="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical uk-text-justify">
                            <li className="uk-active">
                                <a href="#index" className="adjustify">
                                    <ImgLogo2 />
                                </a>
                            </li>
                            <li>
                                <a className="navllink" href="#about-us">
                                    ABOUT US
                                </a>
                            </li>
                            <li>
                                <a className="navllink" href="#brgyoff">
                                    {"BARANGAY OFFICIALS "}
                                </a>
                            </li>
                            <li>
                                <a
                                    className="navllink"
                                    href="#hist"
                                    data-uk-scroll
                                >
                                    HISTORY{" "}
                                </a>
                            </li>
                            <li>
                                <a className="navllink" href="#bc">
                                    BARANGAY CAPTAIN
                                </a>
                            </li>
                            <li>
                                <a className="navllink" href="#popu">
                                    POPULATION
                                </a>
                            </li>
                            <div className="uk-navbar-item">
                                <input
                                    className="uk-input uk-form-width-small"
                                    type="text"
                                    placeholder="Input"
                                />
                                <button
                                    className="uk-button uk-button-primary btn-login"
                                    data-uk-toggle="target: #modal-group-1"
                                >
                                    Login
                                </button>
                            </div>
                        </ul>
                    </div>
                </div>
                <LoginModal />
                <SignupModal />
            </div>
        );
    }
}

export default Nav;
