import React from "react";
import header from "../images/bgren.jpg";
import logo1 from "../images/logo/logoblank.png";
import logo2 from "../images/logo/logolanding.png";
import userblank from "../images/profile/user.png";
import CaptainProfile from "../images/profile/user.png";

function Imgheader() {
	return <img src={header} alt="header" data-uk-cover />;
}

function ImgLogo1() {
	return <img src={logo1} width="120" alt="Logo" />;
}

function ImgLogo2() {
	return <img src={logo2} width="120" alt="BlankLogo" />;
}
function Imguser() {
	return <img src={userblank} alt="Profile" />;
}
function Imgcaptain() {
	return <img src={CaptainProfile} alt="CaptainProfile" data-uk-cover />;
}

export default Imgheader;
export { ImgLogo1, ImgLogo2, Imguser, Imgcaptain };
