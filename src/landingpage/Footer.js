import React, { Component } from "react";

class Footer extends Component {
	render() {
		return (
			<footer id="main-footer">
				<div id="main-footer-fr">
					<div className="uk-text-center">
						Copyright 2017 | ECOLLECT | All Rights Reserved
					</div>
				</div>
			</footer>
		);
	}
}

export default Footer;
