import * as React from "react";
import ProfilePage from "./Components/ProfilePage";
import { BrowserRouter as Router } from "react-router-dom";
import { AuthProvider } from "./config/Auth";
import PrivateRoute from "./config/PrivateRoute";

const App = () => {
    return (
        <AuthProvider>
            <Router>
                <div>
                    <PrivateRoute exact path="/" component={ProfilePage} />
                </div>
            </Router>
        </AuthProvider>
    );
};

export default App;
