import React, { useCallback, useState } from "react";
import { withRouter } from "react-router";
import app from "../config/firebase";

const SignupModal = ({ history }) => {
	const [error, setError] = useState(null);
	const inputRef = React.useRef(null);
	const handleSignUp = useCallback(
		async (event) => {
			event.preventDefault();
			const { email, password, confirmPassword } = event.target.elements;

			try {
				await app
					.auth()
					.createUserWithEmailAndPassword(
						email.value,
						password.value
					);
				//history.push("/");
				inputRef.current.click();
			} catch (error) {
				setError(error.message);
				// if (password !== confirmPassword) {
				// 	setError("Password don't match.");
				// } else {
				// 	setError(error.message);
				// }
			}
		},
		[history]
	);

	return (
		<div id="modal-group-2" className="uk-flex-top" data-uk-modal>
			<div className="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
				<button
					className="uk-modal-close-outside"
					type="button"
					data-uk-close
					ref={inputRef}
				></button>
				<div className="uk-modal-header">
					<h4 className="uk-modal-title uk-text-center">Sign Up</h4>
				</div>
				<div className="uk-modal-body">
					{error && error !== null && (
						<div className="uk-alert-danger" data-uk-alert>
							<p>{error}</p>
						</div>
					)}
					<form onSubmit={handleSignUp}>
						<div className="uk-margin">
							<div className="uk-inline">
								<span
									className="uk-form-icon"
									data-uk-icon="icon: user"
								></span>
								<input
									className="uk-input uk-form-width-large"
									type="text"
									placeholder="Email"
									name="email"
								/>
							</div>
						</div>

						<div className="uk-margin">
							<div className="uk-inline">
								<span
									className="uk-form-icon"
									data-uk-icon="icon: lock"
								></span>
								<input
									className="uk-input uk-form-width-large"
									type="password"
									placeholder="Password"
									name="password"
								/>
							</div>
						</div>
						<div className="uk-margin">
							<div className="uk-inline">
								<span
									className="uk-form-icon"
									data-uk-icon="icon: lock"
								></span>
								<input
									className="uk-input uk-form-width-large"
									type="password"
									placeholder="Retype Password"
									name="confirmPassword"
								/>
							</div>
						</div>
						<div className="uk-margin">
							<button className="uk-button uk-button-small uk-button-primary modal-btn-login uk-align-center">
								Submit
							</button>
						</div>
					</form>
				</div>
				<div className="uk-text-center">
					New user?{" "}
					<a
						href="#modal-group-1"
						data-uk-toggle="target: #modal-group-1"
					>
						Already Registered?
					</a>
				</div>
			</div>
		</div>
	);
};

export default withRouter(SignupModal);
