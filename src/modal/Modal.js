import React, { useCallback, useContext, useState, Component } from "react";
import { withRouter, Redirect } from "react-router";
import app from "../config/firebase";
import { AuthContext } from "../config/Auth";
import * as UIkit from "uikit";

const SignIn = ({ history }) => {
  const [err, setErr] = useState("");

  const inputRef = React.useRef(null);
  const handleLogin = useCallback(
    async (event) => {
      event.preventDefault();
      const { email, password } = event.target.elements;

      try {
        await app
          .auth()
          .signInWithEmailAndPassword(email.value, password.value);
        // history.push("/");
        inputRef.current.click();
      } catch (error) {
        setErr(error.message);
      }
    },
    [history]
  );

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  return (
    <div id="modal-group-1" className="uk-flex-top" data-uk-modal>
      <div className="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
        <button
          className="uk-modal-close-outside"
          type="button"
          data-uk-close
          ref={inputRef}
        ></button>
        <div className="uk-modal-header">
          <h4 className="uk-modal-title uk-text-center">Login</h4>
        </div>
        <div className="uk-modal-body">
          {err && err !== null && (
            <div className="uk-alert-danger" data-uk-alert>
              <p>{err}</p>
            </div>
          )}
          <form className="uk-form" onSubmit={handleLogin}>
            <div className="uk-margin">
              <div className="uk-inline">
                <span className="uk-form-icon" data-uk-icon="icon: user"></span>
                <input
                  className="uk-input uk-form-width-large"
                  type="text"
                  placeholder="Email"
                  name="email"
                  id="userEmail"
                />
              </div>
            </div>

            <div className="uk-margin">
              <div className="uk-inline">
                <span className="uk-form-icon" data-uk-icon="icon: lock"></span>
                <input
                  className="uk-input uk-form-width-large"
                  type="password"
                  placeholder="Password"
                  name="password"
                  id="userPassword"
                />
              </div>
            </div>
            <div className="uk-margin">
              <button
                className="uk-button uk-button-small uk-button-primary modal-btn-login uk-align-center"
                type="submit"
              >
                Login
              </button>
            </div>
            <div className="uk-text-center">Forgot Password</div>
          </form>
        </div>
        <div className="uk-modal-footer">
          <div className="uk-text-center">
            New user?{" "}
            <a href="#modal-group-2" data-uk-toggle="target: #modal-group-2">
              Create an Account
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(SignIn);
